<?php
 
// This is a simplified example, which doesn't cover security of uploaded images. 
// This example just demonstrate the logic behind the process. 
 
require_once '../../Connections/conexao_pdo.php';

if(isset($_FILES['file']))
   {
 
// files storage folder
$dir = '../../uploads/post/';
//$dir = $url_gestor.'assets/uploads/';
//$dir = '/home2/mittel2/public_html/view/gestor/assets/uploads/';


$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

 
if ($_FILES['file']['type'] == 'image/png' 
|| $_FILES['file']['type'] == 'image/jpg' 
|| $_FILES['file']['type'] == 'image/gif' 
|| $_FILES['file']['type'] == 'image/jpeg'
|| $_FILES['file']['type'] == 'image/pjpeg')
{   
    // setting file's mysterious name
    $filename = md5(date('YmdHis')).'.jpg';
    $file = $dir.$filename;

    // copying
    copy($_FILES['file']['tmp_name'], $file);
    move_uploaded_file($_FILES['file']['tmp_name'], $file); //Fazer upload do arquivo
    
    // displaying file    
    $array = array(
        'url' => $url_gestor.'uploads/post/'.$filename
        //'id' => 123
    );
    
    echo stripslashes(json_encode($array));   
    
}
 
 
   }
?>