/**
 * @library Kube Autocomplete
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Autocomplete = function(element, options)
    {
        this.namespace = 'autocomplete';
        this.defaults = {
    		url: false,
    		min: 2,
    		paramName: false,
    		appendForms: false,
    		appendFields: false,
            callbacks: ['set']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Autocomplete.prototype = {
        start: function()
        {
            this.build();

    		this.pos = this.$element.offset();
    		this.elementHeight = this.$element.innerHeight();
    		this.placement = this.calcPlacement();

    		this.timeout = null;
    		this.$element.on('keyup.' + this.namespace, $.proxy(this.open, this));
    	},
    	stop: function()
    	{
    		this.$result.remove();

            this.$element.off('.' + this.namespace);
    		$(document).off('.' + this.namespace);
    		$(window).off('.' + this.namespace);
    	},
    	build: function()
    	{
            this.$result = $('<ul class="autocomplete" />').hide();

            $('body').append(this.$result);
    	},
    	calcPlacement: function()
    	{
            return (($(document).height() - (this.pos.top + this.elementHeight)) < this.$result.height()) ? 'top' : 'bottom';
    	},
    	resize: function()
    	{
        	var width = this.$element.innerWidth();
        	this.$result.innerWidth(width);
    	},
    	getData: function(data)
    	{
            var formdata = new Kube.FormData(this);
            formdata.set(data);

            return formdata.get();
    	},
    	getParamName: function()
    	{
            return (this.opts.paramName) ? this.opts.paramName : this.$element.attr('name');
    	},
    	lookup: function()
    	{
    		var data = this.getParamName() + '=' + this.$element.val();

    		$.ajax({
    			url: this.opts.url,
    			type: 'post',
    			data: this.getData(data),
    			success: $.proxy(this.complete, this)
    		});
    	},
    	complete: function(json)
    	{
			var data = $.parseJSON(json);

			this.$result.html('');

			$.each(data, $.proxy(function(i,s)
			{
				var $li = $('<li>');
				var $a = $('<a href="#" rel="' + i + '">').text(s).on('click', $.proxy(this.set, this));

				$li.append($a);
				this.$result.append($li);

			}, this));

			var top = (this.placement === 'top') ? (this.pos.top - this.result.height() - this.elementHeight) : (this.pos.top + this.elementHeight);

			this.$result.css({ top: top + 'px', left: this.pos.left + 'px' });
			this.$result.show();
			this.active = false;
    	},
    	listen: function(e)
    	{
    		switch(e.which)
    		{
    			case 40: // down
    				e.preventDefault();
    				this.select('next');
    			break;

    			case 38: // up
    				e.preventDefault();
    				this.select('prev');
    			break;

    			case 13: // enter
    				e.preventDefault();
    				this.set();
    			break;

    			case 27: // esc
    				this.close(e);
    			break;

    			default:
    				this.timeout = setTimeout($.proxy(this.lookup, this), 300);
    			break;
    		}
    	},
    	select: function(type)
    	{
    		var $links = this.$result.find('a');
    		var size = $links.length;

    		var $active = this.$result.find('a.active');
    		$active.removeClass('active');

    		var $item = (type === 'next') ? $active.parent().next().children('a') : $active.parent().prev().children('a');
    		if ($item.length === 0)
    		{
    			$item = (type === 'next') ? $links.eq(0) : $links.eq(size-1);
    		}

    		$item.addClass('active');
    		this.active = $item;
    	},
    	set: function(e)
    	{
    		var $el = $(this.active);

    		if (e)
    		{
    			e.preventDefault();
    			$el = $(e.target);
    		}

    		var id = $el.attr('rel');
    		var value = $el.html();

    		this.$element.val(value);

    		this.callback('set', id, value);
    		this.close();
    	},
    	open: function(e)
    	{
    		if (e) e.preventDefault();

    		clearTimeout(this.timeout);

    		var value = this.$element.val();
    		if (value.length >= this.opts.min)
    		{
        		this.resize();
        		$(window).on('resize.' + this.namespace, $.proxy(this.resize, this));
        		$(document).on('click.' + this.namespace, $.proxy(this.close, this));

    			this.$result.addClass('open');
    			this.listen(e);
    		}
    		else
    		{
    			this.close(e);
    		}
    	},
    	close: function(e)
    	{
    		if (e) e.preventDefault();

    		this.$result.removeClass('open');
    		this.$result.hide();

    		$(document).off('.' + this.namespace);
    		$(window).off('.' + this.namespace);
        }
    };

    // Inheritance
    Kube.Autocomplete.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Autocomplete');
    Kube.Plugin.autoload('Autocomplete');

}(Kube));
/**
 * @library Kube Autoresize
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Autoresize = function(element, options)
    {
        this.namespace = 'autoresize';
        this.defaults = {
            minHeight: '50px'
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Autoresize.prototype = {
        start: function()
        {
            this.minHeight = this.getMinHeight();
        	this.paddingHeight = this.getPaddingHeight();

    		this.setMinHeight();
            this.resize();
    		this.$element.on('input.' + this.namespace + ' keyup.' + this.namespace, $.proxy(this.resize, this));
    	},
    	stop: function()
    	{
            this.$element.off('.' + this.namespace);
            this.$element.height('');
    	},
    	resize: function()
    	{
    		if (this.isResize())
    		{
        		var $window = $(window);
        		var scrollPosition = $window.scrollTop();

        		this.$element.height(0).height(this.$element[0].scrollHeight - this.paddingHeight);
        		$window.scrollTop(scrollPosition);
    		}
    	},
    	isResize: function()
    	{
        	return !(this.minHeight && this.$element[0].scrollHeight < this.minHeight);
    	},
    	getMinHeight: function()
    	{
        	return (this.opts.minHeight) ? parseInt(this.opts.minHeight) : false;
    	},
    	getPaddingHeight: function()
    	{
        	return parseInt(this.$element.css('paddingBottom')) + parseInt(this.$element.css('paddingTop'));
    	},
    	setMinHeight: function()
    	{
    		if (this.minHeight !== false)
    		{
    			this.$element.css('min-height', this.minHeight + 'px');
    		}
        }
    };

    // Inheritance
    Kube.Autoresize.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Autoresize');
    Kube.Plugin.autoload('Autoresize');

}(Kube));
/**
 * @library Kube Changer
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Changer = function(element, options)
    {
        this.namespace = 'changer';
        this.defaults = {
            callbacks: ['opened']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Changer.prototype = {
        start: function()
        {
            this.$target = this.getTarget();

            this.setActive();
            this.$target.on('change.' + this.namespace, $.proxy(this.setActive, this));
    	},
    	stop: function()
    	{
        	this.$target.off('.' + this.namespace);
    	},
    	getTarget: function()
    	{
            return (this.isSelect()) ? this.$element : this.$element.find('input[type="radio"]');
    	},
    	getValue: function()
    	{
        	return (this.isSelect()) ? this.$target.val() : this.$target.filter(':checked').val();
    	},
    	getElements: function()
    	{
        	return (this.isSelect()) ? this.$target.find('option') : this.$target;
    	},
    	isSelect: function()
    	{
        	return (this.$element[0].tagName === 'SELECT');
    	},
    	setActive: function(e)
    	{
            var value = this.getValue();

            this.closeAll();
            this.open(value);

            if (e)
            {
                this.callback('opened', value);
            }
    	},
    	open: function(id)
    	{
            $('#' + id).removeClass('hide');
    	},
    	close: function(id)
    	{
            $('#' + id).addClass('hide');
    	},
    	closeAll: function()
    	{
            var $elements = this.getElements();
            var self = this;

            $elements.each(function(i, s)
            {
                self.close($(s).val());
            });
        }
    };

    // Inheritance
    Kube.Changer.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Changer');
    Kube.Plugin.autoload('Changer');

}(Kube));
/**
 * @library Kube Check
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Check = function(element, options)
    {
        this.namespace = 'check';
        this.defaults = {
            target: null,
            classname: 'ch',
            callbacks: ['set']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Check.prototype = {
        start: function()
        {
            this.$checkboxes = $('.' + this.opts.classname);

            this.$checkboxes.on('click.' + this.namespace, $.proxy(this.toggle, this));
            this.$element.on('click.' + this.namespace, $.proxy(this.toggleAll, this));

            this.target = new Kube.Check.Target(this);
            this.refresh(this.target.get());
        },
        stop: function()
        {
            this.$checkboxes.off('.' + this.namespace);
            this.$element.off('.' + this.namespace);

            this.target.stop();
        },
        refresh: function(value)
        {
            var arr = value.split(',');

            this.$checkboxes.prop('checked', false);
            this.$checkboxes.each(function(i, s)
            {
                var $el = $(s);
                $el.prop('checked', (arr.indexOf($el.val()) !== -1));
            });

            if (this.isAll()) this.$element.prop('checked', true);
        },
        toggle: function()
        {
            if (this.isAll()) this.checked(this.$element);
            else              this.unchecked(this.$element);
        },
        toggleAll: function()
        {
            if (this.$element.prop('checked')) this.checkAll();
            else                               this.uncheckAll();
        },
    	isAll: function()
    	{
        	return this.$checkboxes.length === this.$checkboxes.filter(':checked').length;
    	},
    	checkAll: function()
    	{
        	var $elms = this.$checkboxes.add(this.$element);
            this.checked($elms);
    	},
    	uncheckAll: function()
    	{
        	var $elms = this.$checkboxes.add(this.$element);
            this.unchecked($elms);
    	},
    	checked: function($el)
    	{
        	$el.prop('checked', true);
        	this.target.refresh();
    	},
    	unchecked: function($el)
    	{
        	$el.prop('checked', false);
        	this.target.refresh();
    	},
    	set: function(value)
    	{
        	value = this.trimValue(value);

            this.refresh(value);
            this.target.set(value);
            this.callback('set', value);
    	},
    	get: function()
    	{
        	return this.target.get();
    	},
    	trimValue: function(value)
    	{
            return value.split(',').map(function(s) { return s.trim() }).join(',');
    	}
    };

    // Inheritance
    Kube.Check.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Check');
    Kube.Plugin.autoload('Check');

}(Kube));
/**
 * @library Kube Check Target
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Check.Target = function(app)
    {
        this.app = app;
        this.$target = app.$target;
        this.$checkboxes = app.$checkboxes;

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Check.Target.prototype = {
        start: function()
        {
            this.targetCreated = false;
            if (!this.app.hasTarget())
            {
                this.$target = $('<input />');
                this.targetCreated = true;
            }
    	},
    	stop: function()
    	{
        	if (this.targetCreated) this.$target.remove();
        	else                    this.$target.val('');
    	},
        refresh: function()
    	{
        	var value = [];
        	this.$checkboxes.filter(':checked').each(function()
        	{
            	value.push($(this).val());
        	});

        	value = value.join(',');

            this.set(value);
            this.app.callback('set', value);
    	},
    	set: function(value)
    	{
            this.$target.val(value)
    	},
    	get: function()
    	{
        	return this.$target.val();
    	}
    };


}(Kube));
/**
 * @library Kube Combobox
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Combobox = function(element, options)
    {
        this.namespace = 'combobox';
        this.defaults = {
            placeholder: false,
            animationOpen: 'slideDown',
            animationClose: 'slideUp',
            callbacks: ['set']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Combobox.prototype = {
        start: function()
        {
            this.selectedKey = null;
            this.selectedValue = null;

            this.build();
            this.buildPlaceholder();
            this.buildItems();
            this.setDefaultValue();

            this.$source.on('keyup.' + this.namespace, $.proxy(this.clearSelected, this));
            this.$sourceToggle.on('click.' + this.namespace, $.proxy(this.toggle, this));
        },
        stop: function()
        {
            this.$sourceBox.remove();
            this.$element.removeClass('hide')

            $(document).off('.' + this.namespace);
        },
        build: function()
        {
            this.$source = $('<input type="text" />');
            this.$source.addClass(this.buildClassname());

            this.$sourceBox = $('<div class="combobox" />');
            this.$sourceToggle = $('<span class="caret down" />');
            this.$sourceSelect = $('<select />');
            this.$sourceLayer = $('<ul class="combobox-list hide" />');

            this.$element.after(this.$sourceBox);
            this.$element.addClass('hide');

            this.$sourceSelect.addClass('hide');

            this.$sourceBox.append(this.$source);
            this.$sourceBox.append(this.$sourceToggle);
            this.$sourceBox.append(this.$sourceSelect);
            this.$sourceBox.append(this.$sourceLayer);

        },
        buildClassname: function()
        {
            return (this.$element.attr('class')) ? this.$element.attr('class') : '';
        },
        buildPlaceholder: function()
        {
            if (this.opts.placeholder || this.$element.attr('placeholder'))
            {
                var placeholder = (this.opts.placeholder) ? this.opts.placeholder : this.$element.attr('placeholder');

                this.$source.attr('placeholder', placeholder);
            }
        },
        buildItems: function()
        {
            this.$element.find('option').each($.proxy(this.buildListItemsFromOptions, this));
        },
        buildListItemsFromOptions: function(i,s)
        {
            var $el = $(s);
            var val = $el.val();
            if (val === 0)
            {
                return;
            }

            this.createdSourceItem(val, $el.text());
        },
        toggle: function(e)
        {
            if (this.isOpened()) this.close(e);
            else                 this.open(e);
        },
        open: function(e)
        {
            if (e) e.preventDefault();

            if (!this.isOpened())
            {
                this.$sourceLayer.animation(this.opts.animationOpen, $.proxy(this.onOpened, this));
            }
        },
        onOpened: function()
        {
            this.$sourceLayer.addClass('open');

            var value = this.$element.val();

            this.$items = this.$sourceLayer.find('li').removeClass('active');
            this.$selectedElement = this.$items.filter('[rel="' + value + '"]');

            this.$selectedElement.addClass('active');
            this.scrollToItem(this.$selectedElement);

            $(document).on('click.' + this.namespace, $.proxy(this.close, this));
            $(document).on('keydown.' + this.namespace, $.proxy(this.listen, this));
        },
        close: function(e)
        {
            if (e) e.preventDefault();

            if (this.isOpened())
            {
                this.$sourceLayer.animation(this.opts.animationClose, $.proxy(this.onClosed, this));
            }
        },
        onClosed: function()
        {
            this.$sourceLayer.removeClass('open');

            $(document).off('.' + this.namespace);
        },
        isOpened: function()
        {
            return (this.$sourceLayer.hasClass('open'));
        },
        isElementOptionExists: function(value)
        {
            return (this.$element.find('option').filter('[value="' + value + '"]').length === 1);
        },
        isSourceOptionExists: function(value)
        {
            return (this.$sourceLayer.find('li').filter('[rel="' + value + '"]').length === 1);
        },
        createdSourceItem: function(key, value)
        {
            var $item = $('<li />');

            $item.attr('rel', key).text(value);
            $item.on('click.' + this.namespace, $.proxy(this.select, this));

            this.$sourceLayer.append($item);
        },
        createOption: function(key, value)
        {
            return $('<option value="' + key + '" selected="selected">' + value + '</option>');
        },
        addOptionToElement: function(key, value)
        {
            if (this.addedOption) this.addedOption.remove();
            this.addedOption = this.createOption(key, value);

            this.$element.append(this.addedOption);
        },
        setDefaultValue: function()
        {
            var key = this.$element.val();
            var value = this.$element.find('option:selected').text();

            this.setValue(key, value);
        },
        setValue: function(key, value, source)
        {
            var option = this.createOption(key, value);

            if (this.isElementOptionExists(key)) this.$element.val(key);
            else                                 this.addOptionToElement(key, value);

            if (this.isSourceOptionExists(key)) this.$sourceSelect.html(option);

            if (source !== false) this.$source.val(value);

            this.selectedKey = key;
            this.selectedValue = value;
        },
        select: function(e, item)
        {
            if (e) e.preventDefault();

            var $el = $(item || e.target);
            var key = $el.attr('rel');
            var value = $el.text();

            this.$source.val(value);
            this.$element.val(key);

            this.close();

            this.setValue(key, value);
            this.callback('set', { key: key, value: value });
        },
        clearSelected: function()
        {
            var key = this.$source.val();
            if (this.selectedKey === key)
            {
                return;
            }

            this.setValue(key, key, false);
            this.setToNull();

        },
        setToNull: function()
        {
            if (this.$source.val().length === 0)
            {
                this.$element.val(0);
                this.callback('set', { key: 0, value: '' });
            }
        },
        scrollToItem: function($el)
        {
            if ($el.length !== 0)
            {
                this.$sourceLayer.scrollTop(this.$sourceLayer.scrollTop() + $el.position().top - 40);
            }
        },
        get: function()
        {
            return this.selectedKey;
        },
        set: function(key, value)
        {
            this.setValue(key, value);
            this.callback('set', { key: key, value: value });
        },
        listen: function(e)
        {
            var key = e.which;

            if      (key === 38) this.listenUp(e);
            else if (key === 40) this.listenDown(e);
            else if (key === 13) this.listenSelect(e);
            else if (key === 27) this.close(); // esc
        },
        listenUp: function(e)
        {
            if (e) e.preventDefault();

            var $item, $el;

            if (this.$items.hasClass('active'))
            {
                $item = this.$items.filter('li.active');
                $item.removeClass('active');

                var $prev = $item.prev();
                $el = ($prev.length !== 0) ? $el = $prev : this.$items.last();
            }
            else
            {
                $el = this.$items.last();
            }

            $el.addClass('active');
            this.scrollToItem($el);
        },
        listenDown: function(e)
        {
           if (e) e.preventDefault();

           var $item, $el;

           if (this.$items.hasClass('active'))
           {
                $item = this.$items.filter('li.active');
                $item.removeClass('active');

                var $next = $item.next();
                $el = ($next.length !== 0) ? $next : this.$items.first();
           }
           else
           {
                $el = this.$items.first();
           }

           $el.addClass('active');
           this.scrollToItem($el);
        },
        listenSelect: function(e)
        {
            var $item;

            if (this.$items.hasClass('active'))
            {
                $item = this.$items.filter('li.active');
                this.select(e, $item);
            }
        }
    };

    // Inheritance
    Kube.Combobox.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Combobox');
    Kube.Plugin.autoload('Combobox');

}(Kube));
/**
 * @library Kube Editable
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Editable = function(element, options)
    {
        this.namespace = 'editable';
        this.defaults = {
            classname: 'editable',
            focus: false
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Editable.prototype = {
        start: function()
        {
            this.$element.addClass(this.opts.classname).attr('contenteditable', true);

            this.focus();
            this.events();
    	},
    	stop: function()
    	{
            this.$element.removeClass(this.opts.classname).removeAttr('contenteditable');
            this.$element.off('.' + this.namespace);
    	},
    	events: function()
    	{
            this.$element.on('keydown.' + this.namespace, $.proxy(this.keydown, this));
            this.$element.on('paste.' + this.namespace, $.proxy(this.paste, this));
            this.$element.on('blur.' + this.namespace, $.proxy(this.blur, this));
    	},
    	focus: function()
    	{
            if (this.opts.focus) this.$element.focus();
    	},
    	paste: function(e)
    	{
            e.preventDefault();

            var event = (e.originalEvent || e);

            var text = '';
            if (event.clipboardData)
            {
                text = event.clipboardData.getData('text/plain');
                document.execCommand('insertText', false, text);
            }
            else if (window.clipboardData)
            {
                text = window.clipboardData.getData('Text');
                document.selection.createRange().pasteHTML(text);
            }
    	},
    	blur: function(e)
    	{
            if (!this.$element.text().replace(" ", "").length)
            {
                this.$element.empty();
            }
    	},
    	keydown: function(e)
    	{
        	// disable enter key
        	if (e.which === 13) e.preventDefault();
    	}
    };

    // Inheritance
    Kube.Editable.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Editable');
    Kube.Plugin.autoload('Editable');

}(Kube));
/**
 * @library Kube Livesearch
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Livesearch = function(element, options)
    {
        this.namespace = 'livesearch';
        this.defaults = {
    		url: false, // string
    		target: null, // selector
    		min: 2,
    		dropdown: false,
    		paramName: false,
    		appendForms: false,
    		appendFields: false,
    		callbacks: ['result', 'start', 'cancel']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Livesearch.prototype = {
        start: function()
        {
            this.build();
            this.buildClose();

    		this.timeout = null;

    		this.$element.attr('autocomplete', 'off').attr('role', 'search');
    		this.$element.off('keydown.' + this.namespace);
    		this.$element.on('keydown.' + this.namespace, $.proxy(this.search, this));

            if (this.opts.dropdown) this.buildDropdown();
    	},
    	stop: function()
    	{
            this.$element.off('.' + this.namespace);

            if (this.opts.dropdown) this.closeDropdown();
    	},
    	build: function()
    	{
            this.$box = $('<div class="livesearch-box" />');
    		this.$element.after(this.$box);
    		this.$box.append(this.$element);
    	},
    	buildDropdown: function()
    	{
        	this.$target.addClass('livesearch-dropdown');

        	this.resize();
        	$(window).on('resize.' + this.namespace, $.proxy(this.resize, this));
    	},
    	buildClose: function()
    	{
    		this.$close = $('<span class="close small" />').hide();
    		this.$box.append(this.$close);
    		this.$close.off('click.livesearch');
    		this.$close.on('click.livesearch', $.proxy(this.cancel, this));
    	},
    	openDropdown: function()
    	{
			this.$target.removeClass('hide');
			$(document).on('click.' + this.namespace, $.proxy(this.closeDropdown, this));
    	},
    	closeDropdown: function(e)
    	{
        	if (e && $(e.target).closest('.livesearch-dropdown').length !== 0)
        	{
            	return;
        	}

            this.$target.addClass('hide');
            $(document).off('.' + this.namespace);
    	},
    	toggleDropdown: function()
    	{
        	if (length === 0 && this.opts.dropdown) this.closeDropdown();
    	},
    	toggleClose: function(length)
    	{
    		return (length === 0) ? this.$close.hide() : this.$close.show();
    	},
    	getData: function(data)
    	{
            var formdata = new Kube.FormData(this);
            formdata.set(data);

    		return formdata.get();
    	},
    	getParamName: function()
    	{
            return (this.opts.paramName) ? this.opts.paramName : this.$element.attr('name');
    	},
    	search: function(e)
    	{
            if (e && e.which === 27)
        	{
                return this.cancel();
        	}

    		clearTimeout(this.timeout);

    		this.timeout = setTimeout($.proxy(function()
    		{
    			var value = this.$element.val();
    			var data = '';
    			if (value.length > (this.opts.min - 1))
    			{
    				data += '&' + this.getParamName() + '=' + value;
                    this.request(this.getData(data));
    			}

    			this.toggleClose(value.length);
    			this.toggleDropdown(value.length);

    		}, this), 300);
    	},
    	request: function(data)
    	{
          	 this.callback('start');

  		     $.ajax({
    			url: this.opts.url,
    			type: 'post',
    			data: data,
    			success: $.proxy(this.result, this)
    		});
    	},
    	requestCancel: function()
    	{
        	var data = '?' + this.getParamName() + '=';
            data = this.getData(data);

  		     $.ajax({
    			url: this.opts.url,
    			type: 'post',
    			data: data,
    			success: $.proxy(this.resultCancel, this)
    		});
    	},
    	result: function(html)
    	{
            this.$target.html(html);

			if (this.opts.dropdown) this.openDropdown();
            this.callback('result', html);
    	},
    	resultCancel: function(html)
    	{
            this.$target.html(html);

			if (this.opts.dropdown) this.closeDropdown();
            this.callback('cancel', html);
    	},
    	cancel: function(e)
    	{
        	if (e) e.preventDefault();

            this.requestCancel();
    		this.$element.val('');
    		this.$close.hide();
    	},
    	resize: function()
    	{
        	var width = this.$element.innerWidth();
        	var pos = this.$element.offset();
        	var height = this.$element.innerHeight();

            this.$target.innerWidth(width);
        	this.$target.css({
            	top: (pos.top + height) + 'px',
            	left: pos.left + 'px',
        	});
    	}
    };

    // Inheritance
    Kube.Livesearch.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Livesearch');
    Kube.Plugin.autoload('Livesearch');

}(Kube));
/**
 * @library Kube Loader
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Loader = function(element, options)
    {
        this.namespace = 'loader';
        this.defaults = {
            small: false
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Loader.prototype = {
        start: function()
        {
            this.$loader = this.build();
            this.contents = this.$element.contents();

    		this.$element.html(this.$loader);
        },
        stop: function()
        {
            if (this.contents) this.$element.html(this.contents);
        },
        build: function()
        {
    		var $loader = $('<span />').addClass('loader');
    		var $spinner = $('<span />').addClass('loader-spinner');

    		if (this.opts.small) $loader.addClass('small');

    		return $loader.append($spinner);
        }
    };

    // Inheritance
    Kube.Loader.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Loader');

}(Kube));
/**
 * @library Kube Magicquery
 * @author Imperavi LLC
 * @license MIT
 */
(function($)
{
    $.magicquery = function(options)
    {
    	var opts = $.extend({}, options, { show: true });
    	var $element = $('<span />');

    	$element.magicquery(opts);
    };

})(jQuery);


(function(Kube)
{
    Kube.Magicquery = function(element, options)
    {
        this.namespace = 'magicquery';
        this.defaults = {
            show: false,
    		url: false, // string
    		appendForms: false, // selectors
    		appendFields: false, // selectors
    		callbacks: ['complete']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Magicquery.prototype = {
        start: function()
        {
            if (this.opts.show) this.request();
            else                this.$element.on('click.' + this.namespace, $.proxy(this.request, this));
        },
        stop: function()
        {
            this.$element.off('.' + this.namespace);
        },
        request: function(e)
        {
            if (e) e.preventDefault();
  		    $.ajax({
    			url: this.opts.url,
    			type: 'post',
    			data: this.getData(),
    			success: $.proxy(this.complete, this)
    		});
        },
        complete: function(data)
        {
            var response = new Kube.Response();
            var json = response.parse(data);

			this.callback('complete', data, json);

			if (this.opts.show) this.$element.remove();
        },
    	getData: function()
    	{
            var formdata = new Kube.FormData(this);
            formdata.set('');

    		return formdata.get();
    	}
    };

    // Inheritance
    Kube.Magicquery.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Magicquery');
    Kube.Plugin.autoload('Magicquery');

}(Kube));
/**
 * @library Kube Notify
 * @author Imperavi LLC
 * @license MIT
 */
(function($)
{
    $.notify = function(message, options)
    {
        var $element = $('<span />');
        var opts = $.extend({}, { message: message }, options);

    	$element.notify(opts);
    };

})(jQuery);


(function(Kube)
{
    Kube.Notify = function(element, options)
    {
        this.namespace = 'notify';
        this.defaults = {
            target: null,
            message: '',
            delay: 5, // seconds
            callbacks: ['opened', 'closed']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Notify.prototype = {
        start: function()
        {
        	this.stop();
            this.build();
            this.open();
    	},
    	stop: function()
    	{
        	$('.notification-generated').remove();
            $(document).off('.' + this.namespace);
    	},
    	build: function()
    	{
    	    this.$target = $('<div />');
            this.$target.addClass('notification notification-generated hide').appendTo('body');
    	},
    	open: function(e)
    	{
        	if (e) e.preventDefault();

            this.$target.removeClass('hide').addClass('open');
        	this.$target.html(this.opts.message);
            this.$target.on('click.' + this.namespace, $.proxy(this.close, this));
            $(document).on('keyup.' + this.namespace, $.proxy(this.handleKeyboard, this));
            this.timeout = setTimeout($.proxy(this.close, this), this.opts.delay * 1000)
            this.callback('opened');
    	},
    	close: function(e)
    	{
        	if (e) e.preventDefault();

            this.$target.addClass('hide').removeClass('open');
            this.$target.html('');
            this.$target.off('.' + this.namespace);
            $(document).off('.' + this.namespace);
            clearTimeout(this.timeout);
            this.callback('closed');
            this.$element.remove();
        },
    	handleKeyboard: function(e)
    	{
    		if (e.which === 27) this.close();
    	}
    };

    // Inheritance
    Kube.Notify.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Notify');

}(Kube));
/**
 * @library Kube Overlay
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Overlay = function(element, options)
    {
        this.namespace = 'overlay';
        this.defaults = {
            target: null,
            offset: 0,
            callbacks: ['opened', 'closed']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Overlay.prototype = {
        start: function()
        {
            this.$element.on('click.' + this.namespace, $.proxy(this.toggle, this));
    	},
    	stop: function()
    	{
        	this.close();
            this.$element.off('.' + this.namespace);
    	},
    	toggle: function(e)
    	{
        	if (this.isOpened()) this.close(e);
        	else                 this.open(e);
    	},
    	buildClose: function()
    	{
            this.$close = this.$target.find('.close');
            if (this.$close.length === 1)
            {
            	this.$close.css({ top: (16 + this.opts.offset) + 'px' });
            	this.$close.on('click.' + this.namespace, $.proxy(this.close, this));
        	}
    	},
    	buildOffset: function()
    	{
            if (this.opts.offset !== 0)
        	{
            	var paddingBottom = parseInt(this.$target.css('padding-bottom')) + this.opts.offset;
            	this.$target.css({ top: this.opts.offset, 'padding-bottom': paddingBottom + 'px' });
        	}
    	},
    	open: function(e)
    	{
        	if (e) e.preventDefault();

        	$('html').css('overflow', 'hidden');

        	this.buildClose();
            this.buildOffset();

        	this.$target.addClass('overlay').addClass('fixed').removeClass('hide');

            $(document).on('keydown.' + this.namespace, $.proxy(this.handleKeyboard, this));

            this.callback('opened');
    	},
    	close: function(e)
    	{
        	if (e) e.preventDefault();

            $('html').css('overflow', '');

        	this.$target.addClass('hide').removeClass('overlay').removeClass('fixed');

            $(document).off('.' +  this.namespace);
            this.$close.off('.' +  this.namespace);

            this.callback('closed');
    	},
    	handleKeyboard: function(e)
    	{
            if (e.which === 27) this.close();
    	},
    	isOpened: function()
    	{
        	return (this.$target.hasClass('open'));
    	}
    };

    // Inheritance
    Kube.Overlay.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Overlay');
    Kube.Plugin.autoload('Overlay');

}(Kube));
/**
 * @library Kube Progress
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Progress = function(element, options)
    {
        this.namespace = 'progress';
        this.defaults = {
            id: 'progress',
            selector: '.progress',
            value: 100,
            text: '',
            animationOpen: 'fadeIn',
            animationClose: 'fadeOut',
            callbacks: ['open', 'opened', 'close', 'closed']
        };

        // Parent Constructor
        Kube.apply(this, arguments);
    };

    // Functionality
    Kube.Progress.prototype = {
        stop: function()
        {
            $(this.opts.selector).remove();
        },
        build: function()
        {
            this.buildProgress();
            this.buildProgressLine();

            // Append
            this.$element.append(this.$progress);
        },
        buildProgress: function()
        {
            this.$progress = $('<div />');
            this.$progress.attr('id', this.opts.id);
            this.$progress.addClass('progress hide');
            this.$progress.addClass(this.buildPositionClass());
        },
        buildProgressLine: function()
        {
            this.$progressLine = $('<div />');
            this.$progress.append(this.$progressLine);
        },
        buildPositionClass: function()
        {
            return (this.$element[0].tagName === 'BODY') ? 'fixed' : 'absolute';
        },
        open: function()
        {
            this.stop();
            this.build();
            this.update(this.opts.value, this.opts.text);

            this.callback('open');
            this.$progress.animation(this.opts.animationOpen, $.proxy(this.onOpened, this));
        },
        onOpened: function()
        {
            this.callback('opened');
        },
        close: function()
        {
            if (this.$progress)
            {
                this.callback('close');
                this.$progress.animation(this.opts.animationClose, $.proxy(this.onClosed, this));
            }
        },
        onClosed: function()
        {
            this.callback('closed');

            this.$progress.remove();
            this.$progress = null;
        },
        update: function(value, text)
        {
            text = (typeof text === 'undefined') ? '' : text;

            this.$progressLine.css('width', value + '%').text(text);
        }
    };

    // Inheritance
    Kube.Progress.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Progress');

}(Kube));
/**
 * @library Kube Selector
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Selector = function(element, options)
    {
        this.namespace = 'selector';
        this.defaults = {
            triggerSelector: '.selector-trigger',
            callbacks: ['set']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Selector.prototype = {
        start: function()
        {
            this.$trigger = this.$element.find(this.opts.triggerSelector);
            this.$target = this.$element.find('select');

            this.load();
            this.$target.on('change.' + this.namespace, $.proxy(this.load, this));
    	},
    	stop: function()
    	{
            this.$target.off('.' + this.namespace);
    	},
    	load: function(e)
    	{
        	this.$selected = this.$target.find('option:selected');

            var value = this.$selected.val();
            var text = this.$selected.text();

            this.$trigger.text(text);

            if (e)
            {
                this.callback('set', value, text);
            }
        }
    };

    // Inheritance
    Kube.Selector.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Selector');
    Kube.Plugin.autoload('Selector');

}(Kube));
/**
 * @library Kube Slug
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Slug = function(element, options)
    {
        this.namespace = 'slug';
        this.defaults = {
            target: null,
            callbacks: ['set']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Slug.prototype = {
        start: function()
        {
    		this.$element.on('keyup.' + this.namespace, $.proxy(this.pass, this));
    		this.$target.on('keyup.' + this.namespace + ' keydown.' + this.namespace, $.proxy(this.disableKeyup, this));
    	},
    	stop: function()
    	{
    		this.$element.off('.' + this.namespace);
    		this.$target.off('.' + this.namespace);
    	},
    	disableKeyup: function()
    	{
        	this.$element.off('keyup.' + this.namespace);
    	},
    	clean: function(value)
    	{
        	return value.toLowerCase().replace(/[\s_]/gi, '-').replace(/[\.,\/#!$%\^&\*;:"“”'{}=_`~()]/g, '');
    	},
    	pass: function()
    	{
        	var value = this.$element.val();

        	value = this.clean(value);
    		value = this.callback('set', value);

    		this.$target.val(value);
    	}
    };

    // Inheritance
    Kube.Slug.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Slug');
    Kube.Plugin.autoload('Slug');

}(Kube));
/**
 * @library Kube Upload
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Upload = function(element, options)
    {
        this.namespace = 'upload';
        this.defaults = {
            target: false,
            placeholder: 'Drop files here or click to upload',
            height: '80px',
    		progress: false,
    		url: false, // path to upload script
    		multiple: false, // boolean
    		paramName: false,
    		appendForms: false,
    		appendFields: false,
    		imageTypes: ['image/png', 'image/jpeg', 'image/gif'],
    		callbacks: ['fallback', 'start', 'complete']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Upload.prototype = {
        start: function()
        {
            // fallback
    		if (!this.tests())
    		{
    			this.callback('fallback');
    			return;
    		}

    		this.build();
    		this.buildPlaceholder();
            this.buildEvents();
    	},
    	stop: function()
    	{
            this.$uploadbox.remove();
            this.$element.removeClass('hide')
            this.$element.off('.' + this.namespace);
    	},
    	tests: function()
    	{
    		var supported = true;
    		var supports = {};

    		supports.filereader = (typeof FileReader !== 'undefined') ? true : false;
    		supports.dnd = ('draggable' in document.createElement('span')) ? true : false;
    		supports.formdata = (!!window.FormData) ? true : false;

    		for (var api in supports)
    		{
    			if (api === false) supported = false;
    		}

    		return supported;
    	},
    	build: function()
    	{
    		this.$uploadbox = $('<div class="upload-box" />');
    		this.$uploadbox.css({ 'min-height': this.opts.height });

    		this.$element.addClass('hide');
    		this.$element.after(this.$uploadbox);
    	},
    	buildPlaceholder: function()
    	{
    		this.$placeholder = $('<div class="upload-placeholder" />').html(this.opts.placeholder);
    		this.$uploadbox.append(this.$placeholder);
    	},
    	buildEvents: function()
    	{
    		this.$element.on('change.' + this.namespace, $.proxy(this.onChange, this));
    		this.$uploadbox.on('click.' + this.namespace, $.proxy(this.onClick, this));
    		this.$uploadbox.on('drop.' + this.namespace, $.proxy(this.onDrop, this));
    		this.$uploadbox.on('dragover.' + this.namespace, $.proxy(this.onDragOver, this));
    		this.$uploadbox.on('dragleave.' + this.namespace, $.proxy(this.onDragLeave, this));
    	},
    	onClick: function(e)
    	{
    		e.preventDefault();
    		this.$element.click();
    	},
    	onChange: function(e)
    	{
        	this.callback('start');
    		this.send(e, this.$element[0].files);
    	},
    	onDrop: function(e)
    	{
    		e.preventDefault();

    		this.$uploadbox.removeClass('upload-hover upload-error upload-success');
    		this.$uploadbox.addClass('upload-drop');

    		this.callback('start');
    		this.send(e);
    	},
    	onDragOver: function()
    	{
    		this.$uploadbox.addClass('upload-hover');

    		return false;
    	},
    	onDragLeave: function()
    	{
    		this.$uploadbox.removeClass('upload-hover');

    		return false;
    	},
    	getData: function(data)
    	{
        	var formdata = new Kube.FormData(this);
        	formdata.set(data);

        	return formdata.get(true);
    	},
    	getParamName: function()
    	{
            return (this.opts.paramName) ? this.opts.paramName : 'file';
    	},
    	send: function(e, files)
    	{
        	e = e.originalEvent || e;

    		files = (files) ? files : e.dataTransfer.files;

    		var data = new FormData();
    		var len = files.length;
    		var name = this.getParamName();

    		if (len === 1)
    		{
    			data = this.buildData(name, files, data);
    		}
    		else if (len > 1 && this.opts.multiple !== false)
    		{
    			data = this.buildDataMultiple(name, files, data);
    		}

    		this.sendData(this.getData(data));
            this.openProgress();

    	},
    	sendData: function(data)
    	{
    		var xhr = new XMLHttpRequest();
    		xhr.open('POST', this.opts.url);
    		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    		xhr.onreadystatechange = $.proxy(this.complete, this);

    		xhr.send(data);
    	},
    	buildData: function(name, files, data)
    	{
    		data.append(name, files[0]);

    		return data;
    	},
    	buildDataMultiple: function(name, files, data)
    	{
    		var len = files.length;
    		for (var i = 0; i < len; i++)
    		{
    			data.append(name + '-' + i, files[i]);
    		}

    		return data;
    	},
    	complete: function (e)
    	{
    		var data, json;
    		if (e.target.readyState !== 4)
    		{
    			return;
    		}

    		if (e.target.status === 200)
    		{
    			// complete
    			this.$uploadbox.removeClass('upload-hover upload-error upload-drop');
    			this.$uploadbox.addClass('upload-success');

    	        data = this.normalizeJsonString(e.target.responseText);

    	        // validate
    	        var $form = this.$element.closest('form[data-component=validate]');
    	        if ($form.length !== 0)
    	        {
                    $form.validate('parse', data);
    	        }

                // response
                var response = new Kube.Response(this);
    			json = response.parse(data);

    			// upload target
    			if (this.opts.target) $(this.opts.target).uploadtarget('push', json);

    			// callback
    			this.callback('complete', data, json);

    			setTimeout($.proxy(this.completeClear, this), 500);
    			setTimeout($.proxy(this.closeProgress, this), 1000);
    		}
    	},
    	completeClear: function()
    	{
            this.$uploadbox.removeClass('upload-success');
    	},
    	openProgress: function()
    	{
        	if (this.opts.progress) $(this.opts.progress).progress('open');
    	},
    	closeProgress: function()
    	{
            if (this.opts.progress) $(this.opts.progress).progress('close');
    	},
    	normalizeJsonString: function(str)
    	{
    		str = str.replace(/^\[/, '');
    		str = str.replace(/\]$/, '');

    		return str;
        }
    };

    // Inheritance
    Kube.Upload.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Upload');
    Kube.Plugin.autoload('Upload');

}(Kube));
/**
 * @library Kube Validate
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Validate = function(element, options)
    {
        this.namespace = 'validate';
        this.defaults = {
    		trigger: false,
    		errorClass: 'error',
    		send: true,
    		shortcut: false,
    		progress: false,
    		callbacks: ['load', 'send', 'success', 'error']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Validate.prototype = {
        start: function()
        {
            this.disableDefaultValidation();
    		this.setShortcut();

    		return (this.opts.trigger) ? this.initTrigger() : this.initSubmit();
    	},
        stop: function()
    	{
    		this.enableButtons();
    		this.clearErrors();

    		$(this.opts.trigger).off('.' + this.namespace);
    		this.$element.off('.' + this.namespace);
    		$(window).off('.' + this.namespace);
        },
    	disableDefaultValidation: function()
    	{
    		this.$element.attr('novalidate', 'novalidate');
    	},
    	initTrigger: function()
    	{
        	this.$trigger = $(this.opts.trigger);

    		this.$element.submit(function() { return false; });
    		this.$trigger.off('click.' + this.namespace);
    		this.$trigger.on('click.' + this.namespace, $.proxy(this.load, this));
    	},
    	initSubmit: function()
    	{
    		this.$element.on('submit.' + this.namespace, $.proxy(this.load, this));
    	},
    	load: function()
    	{
    		this.callback('load');

    		if (this.opts.send) this.sendForm();

    		return false;
    	},
    	sendForm: function()
    	{
    		if (this.opts.progress) $(this.opts.progress).progress('open');

    		this.disableButtons();
            this.saveCodeMirror();
    		this.callback('send');

    		$.ajax({
    			url: this.$element.attr('action'),
    			type: 'post',
    			data: this.$element.serialize(),
    			success: $.proxy(this.parse, this)
    		});

    		return false;
    	},
    	saveCodeMirror: function()
    	{
            $('.CodeMirror').each(function(i, el)
    		{
    			el.CodeMirror.save();
    		});
    	},
    	parse: function(data)
    	{
    		this.enableButtons();
    		this.clearErrors();

    		if (this.opts.progress) $(this.opts.progress).progress('close');

            var response = new Kube.Response(this);
    		var json = response.parse(data);
    		if (typeof json.type !== 'undefined' && json.type === 'error')
    		{
    			this.validate(json.errors);
    			this.callback('error', json.errors);
    		}
    		else
    		{
    			this.callback('success', json);
    		}
    	},
    	validate: function(errors)
    	{
        	for (var name in errors)
        	{
                var text = errors[name];
            	var $el = this.getField(name);
            	if ($el.length !== 0)
                {
        			this.toggleRedactorError($el, 'add');
        			$el.addClass(this.opts.errorClass);

        			if (text !== '')
        			{
            			this.showErrorText(name, text);
        				this.setFieldEvent($el, name);
        			}
    			}
            }
    	},
    	getField: function(name)
    	{
        	return $(this.$element.find('[name=' + name + ']'));
    	},
    	setFieldEvent: function($el, name)
    	{
    		$el.on(this.getFieldEventName($el) + '.' + this.namespace, $.proxy(function()
    		{
        		this.toggleRedactorError($el, 'remove');
        		this.clearError($el);

    		}, this));
    	},
    	showErrorText: function(name, text)
    	{
        	this.$element.find('#' + name + '-validation-error').addClass(this.opts.errorClass).text(text).show();
    	},
    	toggleRedactorError: function($el, type)
    	{
            var redactor = $el.data('redactor');
    		if (typeof redactor !== 'undefined')
    		{
    			redactor.core.box()[type + 'Class'](this.opts.errorClass);
    		}
    	},
        getFieldEventName: function($el)
        {
    		return ($el[0].tagName === 'SELECT' || $el.prop('type') === 'checkbox' || $el.prop('type') === 'radio') ? 'change' : 'keyup';
        },
    	clearErrors: function()
    	{
    		this.$element.find('.' + this.opts.errorClass).each($.proxy(function(i, el)
    		{
        		this.clearError(el);

            }, this));
    	},
    	clearError: function(el)
    	{
    		this.$element.find('#' + $(el).attr('name') + '-validation-error').removeClass(this.opts.errorClass).html('').hide();
    		$(el).removeClass(this.opts.errorClass).off('.' + this.namespace);
    	},
    	disableButtons: function()
    	{
    		this.$element.find('button').attr('disabled', true);
    	},
    	enableButtons: function()
    	{
    		this.$element.find('button').removeAttr('disabled');
    	},
    	setShortcut: function()
    	{
    		if (this.opts.shortcut)
    		{
        		// ctrl + s or cmd + s
    			$(window).on('keydown.' + this.namespace, $.proxy(this.handleShortcut, this));
    		}
    	},
    	handleShortcut: function(e)
    	{
    		if (((e.ctrlKey || e.metaKey) && e.which === 83))
    		{
    			e.preventDefault();
    			return this.sendForm();
    		}

    		return true;
    	}
    };

    // Inheritance
    Kube.Validate.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Validate');
    Kube.Plugin.autoload('Validate');

}(Kube));
/**
 * @library Kube Visibility
 * @author Imperavi LLC
 * @license MIT
 */
(function(Kube)
{
    Kube.Visibility = function(element, options)
    {
        this.namespace = 'visibility';
        this.defaults = {
            tolerance: 15, // px
            callbacks: ['visible', 'invisible']
        };

        // Parent Constructor
        Kube.apply(this, arguments);

        // Initialization
        this.start();
    };

    // Functionality
    Kube.Visibility.prototype = {
        start: function()
        {
            $(window).on('scroll.' + this.namespace + ' resize.' + this.namespace, $.proxy(this.check, this));
            this.check();
    	},
    	stop: function()
    	{
            $(window).off('.' + this.namespace);
    	},
        check: function()
        {
            var $window = $(window);
            var docViewTop = $window.scrollTop();
            var docViewBottom = docViewTop + $window.height();
            var elemTop = this.$element.offset().top;
            var elemBottom = elemTop + this.$element.height();

            var check = ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= (docViewBottom + this.opts.tolerance)) &&  (elemTop >= docViewTop));
            check ? this.callback('visible') : this.callback('invisible');
        }
    };

    // Inheritance
    Kube.Visibility.inherits(Kube);

    // Plugin
    Kube.Plugin.create('Visibility');
    Kube.Plugin.autoload('Visibility');

}(Kube));