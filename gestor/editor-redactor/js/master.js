
$(function()
{
	if (window.devicePixelRatio >= 1.5)
	{
		var images = $("img.hires");
		for (var i = 0; i < images.length; i++)
		{
			var imageType = images[i].src.substr(-4);
			var imageName = images[i].src.substr(0, images[i].src.length - 4);
			imageName += "@2x" + imageType;
			images[i].src = imageName;
		}
	}

    if ($('#redactor-intro-box').size() !== 0)
    {
	    $('#redactor').redactor({
            lang: 'pt_br',
            imageUpload: 'https://objetivoreplay.com.br/gestor/editor-redactor/scripts/image_upload.php',
            fileUpload: 'https://objetivoreplay.com.br/gestor/editor-redactor/scripts/file_upload.php',
            plugins: ['table', 'video', 'source','scriptbuttons','bufferbuttons','inlinestyle','underline','fontcolor','fontsize','fontfamily','filemanager','imagemanager','fullscreen','alignment'],
            imagePosition: true,
            imageResizable: true
        });
        
    }

    if ($('.plug-redactor-intro-box').size() !== 0)
    {

	    $('.plug-redactor').redactor({
            lang: 'pt_br',
            imageUpload: 'http://localhost/objreplay/gestor/editor-redactor/scripts/image_upload.php',
            fileUpload: 'http://localhost/objreplay/gestor/editor-redactor/scripts/file_upload.php',
            plugins: ['table', 'video', 'source','scriptbuttons','bufferbuttons','inlinestyle','underline','fontcolor','fontsize','fontfamily','filemanager','imagemanager','fullscreen','alignment'],
            imagePosition: true,
            imageResizable: true
        });
        
    }



    if ($('#redactor-intro-box1').size() !== 0)
    {
        $('#redactor1').redactor1({
            lang: 'pt_br',
            imageUpload: 'https://objetivoreplay.com.br/gestor/editor-redactor/scripts/image_upload.php',
            fileUpload: 'https://objetivoreplay.com.br/gestor/editor-redactor/scripts/file_upload.php',
            plugins: ['table', 'video', 'source','scriptbuttons','bufferbuttons','inlinestyle','underline','fontcolor','fontsize','fontfamily','filemanager','imagemanager','fullscreen','alignment'],
            imagePosition: true,
            imageResizable: true
        });
    }



    $('.show-selector-code').on('click', function(e)
    {
        e.preventDefault();

        var $el = $(this);
        var $prev = $el.prev();
        var $selector = $el.parent();
        var $example = $selector.next();
        var $code = $example.next();

        $el.closest('.chart-example').addClass('active-code');

        $prev.removeClass('active');
        $el.addClass('active');

        $example.hide();
        $code.show();
    });

    $('.show-selector-example').on('click', function(e)
    {
        e.preventDefault();

        var $el = $(this);
        var $next = $el.next();
        var $selector = $el.parent();
        var $example = $selector.next();
        var $code = $example.next();

        $el.closest('.chart-example').removeClass('active-code');

        $next.removeClass('active');
        $el.addClass('active');

        $code.hide();
        $example.show();

    });
});


function showGrafsCode(e, el)
{
    e.preventDefault();

    var $view = $(el).parent();
    var $code = $view.next();

    $view.slideUp();
    $code.slideDown();
}

function hideGrafsCode(e, el)
{
    e.preventDefault();

    var $code = $(el).parent();
    var $view = $code.prev();

    $code.slideUp();
    $view.slideDown();
}

        function contarCaracter(valor){ 
			var cont = valor.length;
			
			if(cont == 1 || cont == 0){
				document.getElementById("quantidade").value = cont + " Caracter";
			}else{
				document.getElementById("quantidade").value = cont + " Caracteres";
			}
        }




function getRandomArbitrary(min, max)
{
    return Math.floor(Math.random() * (max - min) + min);
}

function getRandomArray(min, max, len)
{
    len = (typeof len === 'undefined') ? 9 : len;
    var arr = [];
    for (var i = 0; i < len; i++)
    {
        arr.push(getRandomArbitrary(min, max));
    }

    return arr;
}

function addDays(date, days)
{
    date.setDate(date.getDate() + days);

    return date;
}

function getDateArray(days, start)
{
    start = (typeof start === 'undefined') ? 'October 1, 2016' : start;

    var startDate = new Date(start);
    var stopDate = new Date(start);
    stopDate.setDate(stopDate.getDate() + (days-1));

    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate)
    {
        var date = new Date(currentDate);
        dateArray.push(date.getFullYear() + '-' + (parseInt(date.getMonth())+1) + '-' + date.getDate());
        currentDate = addDays(currentDate, 1);

    }

    return dateArray;

}


function showRegForm(id, name, amount, price)
{
    if (id !== 0)
    {

        $('#plan-item-id').val(id);
        $('#selected-item-id').val(id);
        $('#selected-item-name').val(name);
        $('#selected-item-amount').val(amount);
        $('#selected-item-price').val(price);
    }


    $('#reg-form').slideDown(function()
    {
        $('#price-box').hide();
    })
}


function showLoginForm(e)
{
    e.preventDefault();

    $('#login-form').slideDown(function()
    {
        $('#reg-form').hide();
    })
}

function cancelRegForm(e)
{
    e.preventDefault();

    $('#reg-form').slideUp(function()
    {

        $('#reg-form form').validate('clearErrors');
    });

    $('#price-box').show();
}


function cancelLoginForm(e)
{
    e.preventDefault();

    $('#login-form').slideUp(function()
    {
        $('#login-form form').validate('clearErrors');
    });

    $('#reg-form').show();
}
