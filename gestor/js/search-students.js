// $(function(){
// 	$(".input-search-students").on("keyup", function() {
// 	    var value = $(this).val().toLowerCase();
// 	    $(".students-table tr").filter(function() {
// 	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
// 	    });
// 	});
// });

var searchInput = document.querySelector('.input-search-students');
var table = document.querySelector('.students-table');

// searchInput.onkeyup = function() {
//     var filterName = searchInput.value;
//     for (var i = 1; i < table.rows.length; i++) {
//         var findCell = table.rows[i].cells[2].innerText;
//         var result = findCell.toLowerCase().indexOf(filterName) >= 0;
//         table.rows[i].style.display = result ? '' : 'none';
//     }
// };

$('.input-search-students').keyup(function() {
  var nomeFiltro = $(this).val().toLowerCase();
  $('.students-table tbody').find('tr').each(function() {
      var conteudoCelula = $(this).find('td').text();
      var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
      $(this).css('display', corresponde ? '' : 'none');
  });
});

$('.input-search-students-notas').keyup(function() {
  var nomeFiltro = $(this).val().toLowerCase();
  $('.tabela-notas tbody').find('tr').each(function() {
      var conteudoCelula = $(this).find('td').text();
      var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
      $(this).css('display', corresponde ? '' : 'none');
  });
});
