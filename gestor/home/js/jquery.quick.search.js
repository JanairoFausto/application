$(function(){
	$(".input-search").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $(".lista-menus div").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	});
});