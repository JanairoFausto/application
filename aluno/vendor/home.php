<?php
ob_start();

require_once 'Connections/conexao_pdo.php';

// Conectando 
require_once 'Connections/inc_conectado.php';

// Usuário 
require_once 'Connections/usuario.php'; 

$data_atual = date("Y/m/d");
$title_page = "Painel";

$compras_realizadas = "SELECT * FROM vendas WHERE cliente_id='$id' and status_transacao='2' ORDER BY id DESC";
$query_compras_realizadas = $pdo->query( $compras_realizadas );
$row_compras_realizadas = $query_compras_realizadas->fetchAll(PDO::FETCH_ASSOC);
$linhas_compras_realizadas = count($row_compras_realizadas);

$turmas_aluno = "SELECT * FROM alunos_alocados WHERE aluno_id='$id' ORDER BY id DESC";
$query_turmas_aluno = $pdo->query( $turmas_aluno );
$row_turmas_aluno = $query_turmas_aluno->fetchAll(PDO::FETCH_ASSOC);
$linhas_turmas_aluno = count($row_turmas_aluno);

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title>Área do Aluno - <?=$titulo_site;?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Objetivo Replay. A plataforma online de cursos e treinamentos." name="description" />
        <meta content="Janairo Fausto" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- jvectormap -->
        <!--<link href="<?=$url_site;?>assets/libs/jqvmap/jqvmap.min.css" rel="stylesheet" />-->

        <!-- DataTables -->
        <link href="<?=$url_site;?>assets/libs/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=$url_site;?>assets/libs/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        
        <!-- App css -->
        <link href="<?=$url_site;?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$url_site;?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$url_site;?>assets/css/app.min.css" rel="stylesheet" type="text/css" />

        <?php include"icon-favicon.php"; ?>

        <?php include"inc_header_tags.php"; ?>

        <style>
        #novosCursos {
            opacity: 0.5;
            }

        #novosCursos:hover {
            opacity: 1.0;
            }
        
        </style>

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <?php include"inc_navbar_topo.php"; ?>

            <?php include"inc_left_menu_home.php"; ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <!-- start page title -->
                <?php  include"inc_title_home.php"; ?>
                <!-- end page title --> 
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                         
                         <p>&nbsp;</p>


                        <?php include"inc_meio_home.php"; ?>

                        <p>&nbsp;</p>
                                
                    </div> <!-- container -->

                </div> <!-- content -->

                
                <!-- Footer Start -->
                <?php include"inc_footer.php"; ?>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        
        <!-- Right Sidebar -->

        <?php //include"inc_settings_right.php"; ?>
        
        <!-- /Right-bar -->


        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="assets/js/vendor.min.js"></script>

        <!-- KNOB JS -->
        <script src="assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <!-- Chart JS -->
        <script src="assets/libs/chart-js/Chart.bundle.min.js"></script>

        <!-- Jvector map -->
        <!--<script src="assets/libs/jqvmap/jquery.vmap.min.js"></script>
        <script src="assets/libs/jqvmap/jquery.vmap.usa.js"></script>-->
        
        <!-- Datatable js -->
        <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        
        <!-- Dashboard Init JS -->
        <!--<script src="assets/js/pages/dashboard.init.js"></script>-->
        
        <!-- App js -->
        <script src="assets/js/app.min.js"></script>


        <?php ob_end_flush(); ?>

    </body>
</html>