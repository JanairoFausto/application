<div class="row">
    
    <?php if($linhas_compras_realizadas>"0"){ ?>
    <div class="col-md-6">
        <a href="<?=$url_site;?>cursos">
            <div class="card-box widget-chart-one gradient-success bx-shadow-lg">
                <div class="float-left" dir="ltr">
                    <i class="dripicons dripicons-camcorder display-2" style="color:white;"></i>
                </div>
                <div class="widget-chart-one-content text-right">
                    <p class="text-white mb-0 mt-2">CURSOS ONLINE</p>
                    <h3 class="text-white">Clique Aqui</h3>
                </div>
            </div> <!-- end card-box-->
        </a>
    </div>
    <?php } ?> 

    <?php if($linhas_turmas_aluno > "0"){ ?>
    <div class="col-md-6">
        <a href="<?=$url_site;?>ano_letivo">
            <div class="card-box widget-chart-one gradient-danger bx-shadow-lg">
                <div class="float-left" dir="ltr">
                    <i class="dripicons dripicons-graduation display-2" style="color:white;"></i>
                </div>
                <div class="widget-chart-one-content text-right">
                    <p class="text-white mb-0 mt-2">TURMA (AVALIAÇÕES)</p>
                    <h3 class="text-white">Clique Aqui</h3>
                </div>
            </div> <!-- end card-box-->
        </a>
    </div>
    <?php } ?>

</div>