<?php
require_once 'Connections/conexao_pdo.php';

// Conectando 
try { 
     $pdo = new PDO($dsn, $usuario, $senha); 
} catch (PDOException $e) { 
     echo $e->getMessage(); 
     exit(1); 
} 

if( isset($_POST['email']) && isset($_POST['senha']) ){

     // Preparando statement 
     $senha1 = sha1($_POST['senha']);
     $stmt = $pdo->prepare("SELECT * FROM cliente WHERE email = ? AND senha = ?"); 
     $stmt->bindParam(1, $_POST['email'], PDO::PARAM_STR); 
     $stmt->bindParam(2, $senha1, PDO::PARAM_STR); 

     // Executando statement 
     $stmt->execute(); 

     // Obter linha consultada 
     $obj = $stmt->fetchObject(); 

     // Se a linha existe: indicar que esta logado e encaminhar para outro lugar 
     if ($obj) { 
          session_start();
          $_SESSION['identificador'] = $_POST['email'];
          $_SESSION['password'] = $senha1; 

          //header('Location: ./'); 
          echo "<script>document.location.href='".$url_site."'</script>";

     } else { 
          echo "<script>document.location.href='".$url_site."login?erro=true'</script>";
          //echo '<p class="erro">Login/Senha inválidos</p>'; 
          //header('Location: ./errologin.html'); 
     } 

}
?>


<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title>Login - <?=$titulo_site;?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Objetivo Replay. A plataforma online de cursos e treinamentos." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- App css -->
        <link href="<?=$url_site;?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$url_site;?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$url_site;?>assets/css/app.min.css" rel="stylesheet" type="text/css" />

        <?php include"icon-favicon.php"; ?>

        <?php include"inc_header_tags.php"; ?>

    </head>

    <body class="authentication-bg authentication-bg-pattern d-flex align-items-center">

        <!--<div class="home-btn d-none d-sm-block">
            <a href="<?=$url_site;?>"><i class="fas fa-home h2 text-white"></i></a>
        </div>-->
        
        <div class="account-pages w-100 mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center mb-4">
                                    <span><img src="<?=$caminho_logo_login;?>" alt="<?=$title_site;?>" ></span>

                                    <?php if(isset($_GET['erro']) && $_GET['erro']=="true"){echo '<div class="alert alert-danger alert-dismissible fade show mt-lg-3" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>Dados Inválidos!</strong> Digite seu e-mail e senha corretamente.</div>'; } ?>

                                </div>

                                <form action="" class="pt-2" method="post">

                                    <!--<div class="alert alert-primary" role="alert">
                                        Acesse a Plataforma utilizando seu <strong>E-mail e Senha</strong> da <u>Pensar Store</u>.
                                    </div>-->

                                    <div class="form-group mb-3">
                                        <label for="emailaddress">E-mail</label>
                                    
                                          <input class="form-control" type="email" name="email" required="" placeholder="Digite seu e-mail" style="color:black; font-weight:500;">
                                      
                                    </div>

                                    <div class="form-group mb-3">
                                        <a href="<?=$url_site_vendas;?>esqueciasenha" class="text-danger float-right"><small>Esqueci minha senha</small></a>
                                        <label for="password">Senha</label>
                                        <input class="form-control" type="password" required="" id="senha" name="senha" placeholder="Digite sua senha" style="color:black; font-weight:500;">
                                    </div>

                                    <!--<div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                        <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                    </div>-->

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-success btn-block" type="submit"> Entrar </button>
                                    </div>

                                </form>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="<?=$url_site;?>assets/js/vendor.min.js"></script>

        <!-- App js -->
        <script src="<?=$url_site;?>assets/js/app.min.js"></script>
        
    </body>
</html>